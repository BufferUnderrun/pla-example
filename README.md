# PLA-Example #

This application, written with Spring and JavaFX, intended to demonstrate proficiency with 
the learning outcomes of IT-145 at SNHU. Please see syllabus and learning narrative for details 
therein.

## How to Download Binaries ##

https://bitbucket.org/BufferUnderrun/pla-example/downloads/  
(or click Downloads at the left) 

## System Requirements ##

It should run on any machine with the Java 8 JRE installed, but it might also install all the 
prerequisites for you. I've attempted to make it as standalone as possible.

### Author ###

Barrett W Nuzum  
barrett.nuzum@gmail.com

## Copyright ##

Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved  
Unauthorized copying of this file or any file in this repository, via any medium is strictly 
prohibited  
Proprietary and confidential  
