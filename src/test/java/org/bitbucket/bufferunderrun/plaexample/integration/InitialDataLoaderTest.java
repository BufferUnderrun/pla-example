/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.integration;

import static org.junit.Assert.assertEquals;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.bitbucket.bufferunderrun.plaexample.configuration.InitialDataLoader;
import org.bitbucket.bufferunderrun.plaexample.dataaccess.VehicleRepository;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.AbstractVehicle;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.Vehicle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InitialDataLoaderTest {

  @Autowired private InitialDataLoader dataLoader;
  @Autowired private VehicleRepository vehicleRepository;

  @Test
  public void BeansAreInjected() {
    Assert.notNull(dataLoader, "InitialDataLoader is Null");
  };

  @Test
  public void DataLoaderExecuted() {
    long count = vehicleRepository.count();
    Assert.isTrue(count != 0, "Vehicle List was Empty -- thus InitialDataLoader.load was not executed");
    List<Vehicle> cars = StreamSupport.stream(vehicleRepository.findAll().spliterator(), false)
        .sorted(Comparator.comparing(AbstractVehicle::getModelName))
        .collect(Collectors.toList());
    Vehicle car = cars.get(0);
    assertEquals("Accord", car.getModelName());
  }

}
