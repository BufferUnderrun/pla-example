/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.domain.vehicles;

import static org.junit.Assert.assertEquals;

import org.bitbucket.bufferunderrun.plaexample.domain.OutOfFuelException;
import org.junit.Before;
import org.junit.Test;

public class CarTests {

  private Vehicle car;

  @Before
  public void SetUp() {
    this.car = new Vehicle(new Make("Honda"), "Civic", VehicleType.Coupe);
    car.setMaxFuelAmount(20);
  }

  @Test
  public void VerifyMakeAndModelStored() {
    assertEquals("Honda", car.getMake().getName());
    assertEquals("Civic", car.getModelName());
  }

  @Test
  public void DriveReducesFuelLevel() {
    car.fuel(20);
    car.setMilesPerGallon(5);
    car.drive(30);
    assertEquals(14, car.getFuelAmount(), 0.0);
  }

  @Test
  public void DriveIncrementsOdometer() {
    car.fuel(20);
    car.setMilesPerGallon(1);
    car.drive(10);
    assertEquals(10, car.getOdometer(), 0.0);
  }

  @Test
  public void CannotReduceOdometerByDrivingNegativeAmount() {
    car.fuel(20);
    car.setMilesPerGallon(5);
    car.drive(19);
    car.drive(-5);
    assertEquals(19, car.getOdometer(), 0.0);
  }

  @Test
  public void CannotFuelBeyondMaxAmount() {
    car.fuel(30);
    assertEquals(20, car.getMaxFuelAmount(), 0.0);
  }

  @Test
  public void CannotReduceFuelLevelBeyondZero()
  {
    car.fuel(20);
    car.setMilesPerGallon(1);
    try {
      car.drive(25);
    } catch (Exception ex) {

    }
    assertEquals(0.0, car.getFuelAmount(), 0.0);
  }

  @Test
  public void OdometerDoesNotExceedAvailableFuel()
  {
    car.fuel(20);
    car.setMilesPerGallon(1);
    try {
      car.drive(25);
    } catch (Exception ex) {

    }
    assertEquals(20.0, car.getOdometer(), 0.0);
  }

  @Test(expected = OutOfFuelException.class)
  public void DrivingToZeroFuelThrowsException() {
    car.fuel(20);
    car.setMilesPerGallon(1);
    car.drive(25);
  }
}
