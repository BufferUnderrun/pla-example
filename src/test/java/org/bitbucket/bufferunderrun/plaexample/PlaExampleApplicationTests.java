/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample;

import org.bitbucket.bufferunderrun.plaexample.dataaccess.VehicleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlaExampleApplicationTests {

  @Autowired
  private VehicleRepository vehicleRepository;

	@Test
	public void contextLoads() {
	}

	@Test
  public void repositoryWasInstantiated() {
    Assert.notNull(vehicleRepository, "Spring did not inject our Vehicle Repository");
  }

}
