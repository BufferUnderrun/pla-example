/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.map.repository.config.EnableMapRepositories;

import javax.annotation.PostConstruct;

@Configuration
@EnableMapRepositories("org.bitbucket.bufferunderrun.plaexample.dataaccess")
public class AppConfig {

  @Bean
  public InitialDataLoader dataLoader() {
    return new InitialDataLoader();
  }

  @PostConstruct
  public void init() {
    dataLoader().loadData();
  }
}
