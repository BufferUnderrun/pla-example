/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.configuration;

import org.bitbucket.bufferunderrun.plaexample.dataaccess.VehicleRepository;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.Make;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.VehicleType;
import org.bitbucket.bufferunderrun.plaexample.util.VehicleBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class InitialDataLoader {

  @Autowired
  private VehicleRepository vehicleRepository;

  /**
   * Loads Example cars into a vehicleRepository instance.
   * You wouldn't normally do this in a production app -- they would be persisted in a SQL database.
   */
  public void loadData() {
    Make honda = new Make("Honda");
    vehicleRepository.save(VehicleBuilder.build(honda, "Civic", VehicleType.Coupe)
        .withMaxFuelAmount(12)
        .withMilesPerGallon(40)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(honda, "Fit", VehicleType.Sedan)
        .withMaxFuelAmount(10)
        .withMilesPerGallon(35)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(honda, "Accord", VehicleType.Sedan)
        .withMaxFuelAmount(15)
        .withMilesPerGallon(30)
        .getVehicle());

    Make ford = new Make("Ford");
    vehicleRepository.save(VehicleBuilder.build(ford, "Escape", VehicleType.CrossoverUtility)
        .withMaxFuelAmount(12)
        .withMilesPerGallon(22)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(ford, "Mustang", VehicleType.Coupe)
        .withMaxFuelAmount(15)
        .withMilesPerGallon(12)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(ford, "Explorer", VehicleType.SportUtility)
        .withMaxFuelAmount(15)
        .withMilesPerGallon(17)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(ford, "Taurus", VehicleType.Sedan)
        .withMaxFuelAmount(12)
        .withMilesPerGallon(26)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(ford, "F150", VehicleType.PickUp)
        .withMaxFuelAmount(20)
        .withMilesPerGallon(12)
        .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(ford, "Transit", VehicleType.Van)
        .withMaxFuelAmount(20)
        .withMilesPerGallon(26)
        .getVehicle());

    Make subaru = new Make("Subaru");
    vehicleRepository
        .save(VehicleBuilder.build(subaru, "Forester", VehicleType.Wagon)
            .withMaxFuelAmount(16)
            .withMilesPerGallon(24)
            .getVehicle());
    vehicleRepository.save(VehicleBuilder.build(subaru, "Outback", VehicleType.Wagon)
        .withMaxFuelAmount(17)
        .withMilesPerGallon(22)
        .getVehicle());
  }
}
