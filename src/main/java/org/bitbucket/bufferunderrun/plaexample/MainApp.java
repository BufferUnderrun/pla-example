/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication

public class MainApp extends Application {

  private ConfigurableApplicationContext springContext;
  private Parent root;

  @Override
  public void init() throws Exception {
    System.setProperty("glass.accessible.force",
        "false"); // bug workaround - https://stackoverflow.com/a/32597281
    springContext = SpringApplication.run(MainApp.class);
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/pla-example.fxml"));
    fxmlLoader.setControllerFactory(springContext::getBean);
    root = fxmlLoader.load();
  }

  @Override
  public void start(Stage stage) throws Exception {

    Scene scene = new Scene(root, 640, 400);
    scene.getStylesheets().add("/styles/styles.css");

    stage.setTitle("PLA-Example");
    stage.setScene(scene);
    stage.show();

  }

  @Override
  public void stop() throws Exception {
    springContext.stop();
  }


  public static void main(String[] args) {
    launch(MainApp.class, args);
  }

}
