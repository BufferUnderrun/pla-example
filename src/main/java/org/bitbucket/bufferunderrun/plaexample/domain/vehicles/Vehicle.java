/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.domain.vehicles;

import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

@KeySpace("vehicles")
public class Vehicle extends AbstractVehicle {

  @Id private final UUID id = UUID.randomUUID();

  public Vehicle(Make make, String modelName, VehicleType vehicleType) {
    super(make, modelName, vehicleType);
  }
}
