/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.domain.vehicles;

import org.bitbucket.bufferunderrun.plaexample.domain.OutOfFuelException;

public abstract class AbstractVehicle implements IVehicle {

  private Make make;
  private String modelName;
  private VehicleType vehicleType;

  private int numWheels = 4;
  private double milesPerGallon = 0;

  private double odometer = 0;
  private double fuelAmount = 0;
  private double maxFuelAmount = 0;

  protected AbstractVehicle(Make make, String modelName, VehicleType vehicleType) {
    this.make = make;
    this.modelName = modelName;
    this.vehicleType = vehicleType;
  }

  public int getNumWheels() {
    return numWheels;
  }

  public void setNumWheels(int numWheels) {
    this.numWheels = numWheels;
  }

  public double getMaxFuelAmount() {
    return maxFuelAmount;
  }

  public void setMaxFuelAmount(double maxFuelAmount) {
    this.maxFuelAmount = maxFuelAmount;
  }

  public double getMilesPerGallon() {
    return milesPerGallon;
  }

  public void setMilesPerGallon(double milesPerGallon) {
    this.milesPerGallon = milesPerGallon;
  }

  public void setInitialFuelAmount(double fuelAmount) {
    this.fuelAmount = fuelAmount;
  }

  public double getOdometer() {
    return odometer;
  }

  public double getFuelAmount() {
    return fuelAmount;
  }

  @Override
  public void drive(double miles) {
    double amountOfFuelRequested = Math.max(0.0, miles) / milesPerGallon;
    double amountOfFuelToUse = Math.min(amountOfFuelRequested, fuelAmount);
    fuelAmount = Math.max(0.0, fuelAmount - amountOfFuelToUse);
    odometer += amountOfFuelToUse * milesPerGallon;
    if (fuelAmount == 0.0) {
      throw new OutOfFuelException();
    }
  }

  @Override
  public void fuel(double gallons) {
    this.fuelAmount = Math.max(gallons + fuelAmount, maxFuelAmount);
  }

  public Make getMake() {
    return make;
  }

  public String getModelName() {
    return modelName;
  }

  public VehicleType getVehicleType() {
    return vehicleType;
  }

  @Override
  public String toString() {
    return this.getMake().getName() + " " + this.getModelName();
  }

  /**
   * Resets the odometer and refuels the vehicle.
   * For use when the user changes the selected vehicle.
   */
  public void reset() {
    this.fuelAmount = this.maxFuelAmount;
    this.odometer = 0;
  }
}
