/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.domain.vehicles;
import java.util.UUID;
import org.springframework.data.annotation.Id;

public interface IVehicle {

  @Id UUID id = UUID.randomUUID();

  void drive(double miles);

  void fuel(double gallons);

}
