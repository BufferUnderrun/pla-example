/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.converters;

import java.util.List;

import javafx.util.StringConverter;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.Vehicle;

public class VehicleModelToStringConverter extends StringConverter<Vehicle> {

  private final List<Vehicle> vehicleList;

  public VehicleModelToStringConverter(List<Vehicle> vehicleList) {
    this.vehicleList = vehicleList;
  }

  @Override
  public String toString(Vehicle object) {
    return object.getModelName();
  }

  @Override
  public Vehicle fromString(String string) {
    return vehicleList.stream().filter((item) -> item
        .getModelName()
        .equals(string))
        .findFirst()
        .orElse(null);
  }
}
