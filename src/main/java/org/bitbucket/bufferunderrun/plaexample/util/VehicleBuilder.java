/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample.util;

import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.*;

/**
* An implementation of the Builder pattern to provide a Fluent interface
* to create a new Vehicle instance.
 * */
public class VehicleBuilder {

  private Vehicle vehicle;

  private VehicleBuilder(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  /**
   * Starts a new instance of this Builder class.
   * @param make - the make of the car to be built
   * @param modelName - the modelname of the car to be built, as a string
   * @param type - the type of the vehicle to be built
   * @return the VehicleBuilder instance
   */
  public static VehicleBuilder build(Make make, String modelName, VehicleType type) {
    Vehicle vehicle = new Vehicle(make, modelName, type);
    return new VehicleBuilder(vehicle);
  }

  /**
   * Sets both the maximum fuel amount *and* assumes the car is fully fueled.
   * @param gallons - the capacity of the fuel tank in gallons
   * @return VehicleBuilder instance
   */
  public VehicleBuilder withMaxFuelAmount(double gallons) {
    this.vehicle.setMaxFuelAmount(gallons);
    this.vehicle.setInitialFuelAmount(gallons);
    return this;
  }

  /**
   * Sets the miles per gallon that the car consumes.
   * @param mpg - the miles per gallon
   * @return the VehicleBuilder instance
   */
  public VehicleBuilder withMilesPerGallon(double mpg) {
    this.vehicle.setMilesPerGallon(mpg);
    return this;
  }

  /**
   * This is how you get the instance of the Vehicle that we have built.
   * @return the Vehicle that has been built.
   */
  public Vehicle getVehicle() {
    return this.vehicle;
  }
}
