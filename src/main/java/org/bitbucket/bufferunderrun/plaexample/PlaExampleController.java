/* Copyright (C) 2017 Barrett W Nuzum - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Barrett W Nuzum, <barrett.nuzum@gmail.com>, December 2017
*/
package org.bitbucket.bufferunderrun.plaexample;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import org.bitbucket.bufferunderrun.plaexample.converters.VehicleModelToStringConverter;
import org.bitbucket.bufferunderrun.plaexample.dataaccess.VehicleRepository;
import org.bitbucket.bufferunderrun.plaexample.domain.OutOfFuelException;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.Vehicle;
import org.bitbucket.bufferunderrun.plaexample.domain.vehicles.VehicleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class PlaExampleController {

  private static final Logger log = LoggerFactory.getLogger(PlaExampleController.class);

  @Autowired
  private VehicleRepository vehicleRepository;

  private List<Vehicle> vehicleList;
  private Property<Vehicle> selectedVehicle;

  @FXML
  private Button selectVehicleButton;
  @FXML
  private Button clearSelectionButton;
  @FXML
  private Pane selectedVehiclePane;
  @FXML
  private Label selectedVehicleLabel;

  @FXML
  private ComboBox<String> makeCombo;
  @FXML
  private ComboBox<VehicleType> typeCombo;
  @FXML
  private ComboBox<Vehicle> modelCombo;

  @FXML
  public TextField mpgField;
  @FXML
  public TextField curFuelField;
  @FXML
  public TextField maxFuelField;
  @FXML
  public TextField odometerField;
  @FXML
  public TextField milesToDriveField;

  @FXML
  public Button driveButton;

  @FXML
  private MenuItem menuQuit;
  @FXML
  private MenuItem menuAbout;

  @FXML
  public void initialize() {
    menuQuit.setOnAction((actionEvent) -> Platform.exit());
    menuAbout.setOnAction(this::handleShowAbout);

    selectedVehicle = new SimpleObjectProperty<Vehicle>(null);
    selectedVehicle.addListener(this::updateFields);

    driveButton.setOnAction(this::driveTheCar);

    vehicleList = StreamSupport.stream(vehicleRepository.findAll().spliterator(), false).collect(
        Collectors.toList());

    makeCombo.setItems(FXCollections.observableArrayList(vehicleList
        .stream()
        .map((car) -> car.getMake().getName())
        .distinct()
        .sorted()
        .collect(Collectors.toList())));

    makeCombo.setOnAction(this::handleMakeSelected);
    makeCombo.setDisable(false);
  }

  private void driveTheCar(ActionEvent actionEvent) {
    try {
      selectedVehicle.getValue().drive(Double.parseDouble(milesToDriveField.getText()));
    } catch (OutOfFuelException ex) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setContentText("You are out of gas!");
      alert.showAndWait();
    }
    updateFields(selectedVehicle);
  }

  private void updateFields(Observable observable) {
    Vehicle theCar = selectedVehicle.getValue();
    if (theCar == null) {
      return;
    }
    selectedVehicleLabel.setText(theCar.toString());

    mpgField.setText(String.format("%.2f", theCar.getMilesPerGallon()));
    curFuelField.setText(String.format("%.2f", theCar.getFuelAmount()));
    maxFuelField.setText(String.format("%.2f", theCar.getMaxFuelAmount()));
    odometerField.setText(String.format("%.2f", theCar.getOdometer()));
    milesToDriveField.setText("0");
    driveButton.setDisable(theCar.getFuelAmount() <= 0);
  }

  private void handleShowAbout(ActionEvent actionEvent) {
    Alert alert = new Alert(AlertType.INFORMATION);
    alert.setTitle("About");
    alert.setHeaderText("PLA-Example v1.0");
    alert.setContentText(
        "An example application created for CAEL evaluators to see if I should get credit "
            + "for IT-145 at SNHU."
            + "\n\nBy Barrett Nuzum");

    alert.showAndWait();
  }

  private void handleMakeSelected(ActionEvent actionEvent) {
    typeCombo.setItems(FXCollections.observableArrayList(vehicleList
        .stream()
        .filter((car) -> car.getMake().getName().equals(makeCombo.getValue()))
        .map((car) -> car.getVehicleType())
        .distinct()
        .sorted()
        .collect(Collectors.toList())));

    typeCombo.setOnAction(this::handleTypeSelected);
    typeCombo.setDisable(false);
  }

  private void handleTypeSelected(ActionEvent actionEvent) {
    modelCombo.setItems(FXCollections.observableArrayList(vehicleList
        .stream()
        .filter((car) -> car.getMake().getName().equals(makeCombo.getValue())
            && car.getVehicleType().equals(typeCombo.getValue()))
        .filter(distinctByKey((car) -> car.getModelName()))
        .sorted((Comparator.comparing(Vehicle::getModelName)))
        .collect(Collectors.toList())));

    modelCombo.setConverter(new VehicleModelToStringConverter(modelCombo.getItems()));
    modelCombo.setDisable(false);
    modelCombo.setOnAction(this::handleModelSelected);
  }

  private void handleModelSelected(ActionEvent actionEvent) {
    selectVehicleButton.setDisable(false);
    clearSelectionButton.setDisable(false);

    selectVehicleButton.setOnAction(this::handleVehicleSelected);
    clearSelectionButton.setOnAction(this::handleVehicleClear);
  }

  private void handleVehicleSelected(ActionEvent actionEvent) {
    selectedVehicle.setValue(modelCombo.getValue());
    selectedVehiclePane.setVisible(true);
    selectVehicleButton.setDisable(true);
    clearSelectionButton.setDisable(false);
  }

  private void handleVehicleClear(ActionEvent actionEvent) {
    if (selectedVehicle.getValue() != null) {
      selectedVehicle.getValue().reset();
    }
    selectedVehicle.setValue(null);
    selectedVehiclePane.setVisible(false);
    clearSelectionButton.setDisable(true);
    selectVehicleButton.setDisable(false);
  }


  /*
  * This convenience method was taken from StackOverflow.
  * https://stackoverflow.com/a/27872852
  */
  public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = ConcurrentHashMap.newKeySet();
    return tee -> seen.add(keyExtractor.apply(tee));
  }

}
